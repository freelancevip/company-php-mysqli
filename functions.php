<?php
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'company');
define('DB_PORT', 3306);

$mysqli = null;

/**
 * @return mysqli object
 */
function getDb()
{
    global $mysqli;
    if (null != $mysqli) {
        return $mysqli;
    }
    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME, DB_PORT);
    if ($mysqli->connect_errno) {
        trigger_error("Failed to conenct to to MySQL: " . $mysqli->connect_error, E_USER_ERROR);
    }
    return $mysqli;
}

/**
 * @return array
 */
function getCompanies()
{
    $mysqli = getDb();
    $result = $mysqli->query("SELECT title FROM companies");
    $rows = [];
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    $mysqli->close();
    return $rows;
}

/**
 * @param $data
 * @return string
 */
function addCompany($data)
{
    $mysqli = getDb();
    if (companyExists($data)) {
        return "Компания [{$data['title']}] уже существует!";
    }
    $stmt = $mysqli->prepare('INSERT INTO companies VALUES(NULL, ?)');
    $stmt->bind_param("s", $data['title']);
    $stmt->execute();
    return "Компания [{$data['title']}] успешно добавлена!";
}

/**
 * @param $data
 * @return bool
 */
function companyExists($data)
{
    $mysqli = getDb();
    $stmt = $mysqli->prepare('SELECT * FROM companies WHERE title = ?');
    $stmt->bind_param("s", $data['title']);
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->num_rows > 0) {
        return true;
    }
    $stmt->close();
    return false;
}
