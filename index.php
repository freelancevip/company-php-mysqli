<?php include 'functions.php' ?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Company manager</title>
</head>
<body>
<div class="container" style="max-width: 400px;margin: 0 auto;">
    <?php
    $companies = getCompanies();
    if ($companies) : ?>
        <div class="company-list">
            <h1>Список компаний</h1>
            <ul>
                <?php
                foreach ($companies as $company) :
                    ?>
                    <li><?= $company['title'] ?></li>
                    <?php
                endforeach;
                ?>
            </ul>
        </div>
    <?php endif ?>
    <div class="company-actions">
        <div id="message"></div>
        <form action="#" id="form">
            <p><input type="text" name="title" placeholder="Введите название"></p>
            <p>
                <button id="add">Добавить</button>
            </p>
        </form>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $("#add").click(function (e) {
            e.preventDefault();
            $.post("./add.php", $("#form").serializeArray(), function (data) {
                $("#message").text(data);
            });
            return false;
        })
    })
</script>
</body>
</html>